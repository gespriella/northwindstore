﻿using Northwind.Store.Model.DTOs;
using Northwind.Store.Model.Entities;
using Northwind.Store.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Northwind.Store.Data.Implementations
{
    public class CustomersRepository : GenericRepository<Customer>, ICustomersRepository
    {
        public CustomersRepository(NWContext context) : base(context)
        {
        }
        public Customer GetById(string id)
        {
            return _context.Set<Customer>().Find(id);
        }
        public IQueryable<CustomerLab1> GetOrderedCustomersLab1()
        {
            return _context.Customers.OrderBy(c => c.CompanyName)
                .Select(s => new CustomerLab1
                {
                    CustomerId = s.CustomerId,
                    CompanyName = s.CompanyName,
                    ContactName = s.ContactName,
                    Country = s.Country
                });
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Northwind.Store.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.Store.Data.Implementations
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly NWContext _context;

        public GenericRepository(NWContext context)
        {
            this._context = context;
        }

        public T Add(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }

        public T GetById(int id)
        {
            return  _context.Set<T>().Find(id);
        }

        public IQueryable<T> GetAll()
            => _context.Set<T>();

        public IReadOnlyList<T> List(Expression<Func<T, bool>> expression)
            => _context.Set<T>().Where(expression).ToList();

        public void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Northwind.Store.Data;
using System;
using System.IO;
using System.Linq;

namespace Northwind.Store.UI.Shell
{
    class Program
    {
        static void Main(string[] args)
        {
                var optionsBuilder = new DbContextOptionsBuilder<NWContext>();
                optionsBuilder.UseSqlServer(GetConnectionString("NW"));

                //Creación directa del context object
                NWContext context = new NWContext(optionsBuilder.Options);

                var customers = context.Customers.OrderBy(c => c.CompanyName).Select(s => new { s.CompanyName, s.ContactName, s.Country });
                Console.WriteLine(
                    $"{String.Format("{0,-40}", "Company Name:")}" +
                    $"{String.Format("{0,-30}", "Contact Name:")}" +
                    $"Country:");

                foreach (var item in customers)
                {
                    Console.WriteLine(
                        $"{String.Format("{0,-40}", item.CompanyName)}" +
                        $"{String.Format("{0,-30}", item.ContactName)}" +
                        $"{item.Country}");
                }
                Console.ReadLine();
        }


        static string GetConnectionString(string Name) {
            var builder = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            return configuration.GetConnectionString(Name);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Northwind.Store.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Northwind.Store.UI.Demo1.ViewModels
{
    public class ProductsIndexModel
    {
        public IEnumerable<Product> Products { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public string Filter { get; set; }

        [BindProperty(SupportsGet = true)]
        public int CurrentPage { get; set; } = 1;

        public int ItemsPerPage { get; set; } = 10;
        public int ItemsCount { get; set; }
        public int TotalPages => (int)Math.Ceiling(decimal.Divide(ItemsCount, ItemsPerPage));

        
    }
}

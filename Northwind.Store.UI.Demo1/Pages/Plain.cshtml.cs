﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Northwind.Store.Model.DTOs;
using Northwind.Store.Model.Interfaces;

namespace Northwind.Store.UI.Demo1.Pages
{
    public class PlainModel : PageModel
    {

        private readonly ICustomersRepository _customers;
        
        [BindProperty(SupportsGet = true)]
        public int CurrentPage { get; set; } = 1;
        public int ItemsPerPage { get; set; } = 10;
        public int ItemsCount { get; set; }
        public int TotalPages => (int)Math.Ceiling(decimal.Divide(ItemsCount, ItemsPerPage));

        public List<CustomerLab1> CustomerList;
        public PlainModel(ICustomersRepository customers)
        {
            _customers = customers;
        }

        public void OnGet()
        {
            CustomerList = _customers.GetOrderedCustomersLab1()
                .Skip((CurrentPage-1) * ItemsPerPage)
                .Take(ItemsPerPage)
                .ToList();
            ItemsCount = _customers.GetAll().Count();
        }
        public IActionResult OnGetCustomerDetailsDialog(string customerId) {
            return Partial("_CustomerDetailsDialog", _customers.GetById(customerId));
        }
    }
}
    

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Northwind.Store.Model.DTOs;
using Northwind.Store.Model.Interfaces;

namespace Northwind.Store.UI.Demo1.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly ICustomersRepository _customers;

        public List<CustomerLab1> CustomerList;
        public IndexModel(ILogger<IndexModel> logger, ICustomersRepository customers)
        {
            _logger = logger;
            _customers = customers;
        }

        public void OnGet()
        {
            CustomerList = _customers.GetOrderedCustomersLab1().ToList();
        }

    }
}
    

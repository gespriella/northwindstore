﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Northwind.Store.Model.DTOs
{
    public class CustomerLab1
    {
        public string CustomerId { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string Country { get; set; }
    }
}

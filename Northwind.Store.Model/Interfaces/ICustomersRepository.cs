﻿using Northwind.Store.Model.DTOs;
using Northwind.Store.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Northwind.Store.Model.Interfaces
{
    public interface ICustomersRepository : IGenericRepository<Customer>
    {
        Customer GetById(string id);
        IQueryable<CustomerLab1> GetOrderedCustomersLab1();
    }
    
}

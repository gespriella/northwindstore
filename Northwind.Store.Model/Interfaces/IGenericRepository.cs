﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.Store.Model.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        T GetById(int id);
        IQueryable<T> GetAll();
        IReadOnlyList<T> List(Expression<Func<T, bool>> expression);
        T Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
    
}
